package com.atlassian.event.config;

import java.util.List;
import javax.annotation.Nonnull;

import com.atlassian.event.spi.ListenerHandler;

/**
 * Specifies a listener handler configuration to use
 */
public interface ListenerHandlersConfiguration {
    @Nonnull
    List<ListenerHandler> getListenerHandlers();
}
