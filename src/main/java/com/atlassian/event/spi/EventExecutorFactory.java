package com.atlassian.event.spi;

import java.util.concurrent.Executor;
import javax.annotation.Nonnull;

/**
 * A factory to create executors for asynchronous event handling
 */
public interface EventExecutorFactory {
    /**
     * @return a new {@link Executor}
     */
    @Nonnull
    Executor getExecutor();
}
