package com.atlassian.event.api;

import javax.annotation.Nonnull;

/**
 * Interface to publish events. It allows the decoupling of listeners which handle events and
 * publishers which dispatch events.
 *
 * @see EventListener annotation which can be used to indicate event listener methods
 * @since 2.0
 */
public interface EventPublisher extends EventListenerRegistrar {
    /**
     * Publish an event that will be consumed by all listeners which have registered to receive it.
     * Implementations must dispatch events to listeners which have a public method annotated with
     * {@link EventListener} and one argument which is assignable from the event type (i.e. a superclass
     * or interface implemented by the event object).
     *
     * This method should process all event listeners, despite any errors or exceptions that are generated
     * as a result of dispatching the event.
     *
     * <h4>Default order</h4>
     * The order in which registered listeners are invoked is predictable. Listeners will be invoked for listeners
     * registered on the object itself, then listeners on the parent class, then the grandparent and so on until
     * finally all listeners for java.lang.Object are invoked.
     * <p>
     * After walking the class hierarchy the interface listeners are invoked, again from the most specific interface
     * first.  Note that the ordering within a specific event type is not guaranteed. If there are multiple registered
     * listeners for IssueEvent, then they will be invoked in the order of registration. It is however guaranteed that
     * a listener for IssueEvent will be invoked before a listener for Event
     *
     * <h4>Custom order</h4>
     * The default order can be overridden with {@link EventListener#order()}. Event listeners with order defined are
     * overriding default rules and will be processed in ascending order of the {@link EventListener#order()} value.
     * Default ordering is preserved when processing event listeners with the same {@link EventListener#order()} value.
     *
     * There is a soft rule for plugin event listeners to use order values in range [-1000, 1000] if needed.
     * Values in following ranges: [{@link Integer#MIN_VALUE}, -1000) and (1000, {@link Integer#MAX_VALUE}] are reserved
     * for Atlassian product internal event listeners.
     *
     * @param event the event to publish
     * @throws NullPointerException if the event is {@code null}
     */
    void publish(@Nonnull Object event);
}
