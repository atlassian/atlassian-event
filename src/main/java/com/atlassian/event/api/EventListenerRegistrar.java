package com.atlassian.event.api;

import javax.annotation.Nonnull;

public interface EventListenerRegistrar {
    /**
     * Register a listener to receive events.
     * <p>
     * All implementations must support registration of listeners where event handling methods are indicated by the
     * {@link com.atlassian.event.api.EventListener} annotation.
     *
     * @param listener The listener that is being registered
     * @throws NullPointerException     if the listener is {@code null}
     * @throws IllegalArgumentException if the parameter is not found to be an actual listener
     * @see com.atlassian.event.api.EventListener annotation which can be used to indicate event listener methods
     */
    void register(@Nonnull Object listener);

    /**
     * Un-register a listener so that it will no longer receive events.
     * <p>
     * If the given listener is not registered, nothing will happen.
     *
     * @param listener The listener to un-register
     * @throws NullPointerException if the listener is {@code null}
     */
    void unregister(@Nonnull Object listener);

    /**
     * Un-register all listeners that this registrar knows about.
     */
    void unregisterAll();
}
