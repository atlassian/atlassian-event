package com.atlassian.event.internal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.MapMaker;

import com.atlassian.event.spi.ListenerInvoker;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;

/**
 * Calculate list of {@code ListenerInvoker}(s) for an event.
 */
class ListenerRegistry {
    private static final Logger log = LoggerFactory.getLogger(ListenerRegistry.class);

    private final AtomicInteger registerSeq;

    /**
     * We always want an {@link InvokerRegistry} created for any class requested, even if it is empty.
     * <b>Warning:</b> We need to use weakKeys here to ensure plugin event classes are GC'd, otherwise we leak...
     */
    private final ConcurrentMap<Class<?>, InvokerRegistry> invokerRegistries =
            new MapMaker().weakKeys().makeMap();

    /**
     * Caches an expensive computation of {@link ListenerInvoker}(s) for each event class.
     * <b>Warning:</b> We need to use weakKeys here to ensure plugin event classes are GC'd, otherwise we leak...
     */
    private final AtomicReference<ConcurrentMap<Class<?>, Iterable<ListenerInvoker>>> invokerCache =
            new AtomicReference<>(createInvokerCacheInstance());

    /**
     * Gets the {@link ListenerInvoker invokers} for a listener
     */
    private final InvokerBuilder invokerBuilder;

    ListenerRegistry(final InvokerBuilder invokerBuilder) {
        this.invokerBuilder = invokerBuilder;
        this.registerSeq = new AtomicInteger();
    }

    void register(final Object listener) {
        invokerBuilder.build(listener).forEach(invoker -> register(listener, invoker));
        clearInvokerCache();
    }

    Iterable<ListenerInvoker> findListenerInvokers(final Object event) {
        return invokerCache.get().computeIfAbsent(requireNonNull(event).getClass(), eventClass -> {
            List<ComparableListenerInvoker> unsorted = new ArrayList<>();
            AtomicInteger classHierarchyOrder = new AtomicInteger();
            for (Class<?> eventType : ClassUtils.findAllTypes(eventClass)) {
                invokerRegistries
                        .getOrDefault(eventType, InvokerRegistry.EMPTY)
                        .forEach(x -> unsorted.add(new ComparableListenerInvoker(
                                x.getListenerInvoker(), classHierarchyOrder.get(), x.getOrder())));
                classHierarchyOrder.incrementAndGet();
            }
            return unsorted.stream().sorted().distinct().collect(toList());
        });
    }

    private void register(final Object listener, final ListenerInvoker invoker) {
        Set<Class<?>> supportedEventTypes = invoker.getSupportedEventTypes();
        if (supportedEventTypes.isEmpty()) {
            // if supported classes is empty, then all events are supported.
            supportedEventTypes = Collections.singleton(Object.class);
        }
        for (final Class<?> eventClass : supportedEventTypes) {
            EventPublisherImpl.debugRegistration.ifPresent(classPrefix -> {
                if (eventClass.getName().startsWith(classPrefix)) {
                    log.warn("Listener registered event '{}' -> invoker {}", eventClass, invoker);
                    if (EventPublisherImpl.debugRegistrationLocation) {
                        log.warn("Registered from", new Exception());
                    }
                }
            });
            invokerRegistries
                    .computeIfAbsent(eventClass, k -> new InvokerRegistry())
                    .add(listener, invoker, registerSeq.get());
        }
        registerSeq.getAndIncrement();
    }

    void remove(final Object listener) {
        invokerRegistries.forEach((k, registry) -> registry.remove(listener));
        clearInvokerCache();
    }

    void clear() {
        invokerRegistries.clear();
        clearInvokerCache();
    }

    private void clearInvokerCache() {
        invokerCache.set(createInvokerCacheInstance());
    }

    private static ConcurrentMap<Class<?>, Iterable<ListenerInvoker>> createInvokerCacheInstance() {
        return new MapMaker().weakKeys().makeMap();
    }
}
