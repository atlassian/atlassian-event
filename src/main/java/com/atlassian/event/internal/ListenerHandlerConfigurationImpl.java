package com.atlassian.event.internal;

import java.util.List;
import javax.annotation.Nonnull;

import com.google.common.collect.Lists;

import com.atlassian.event.config.ListenerHandlersConfiguration;
import com.atlassian.event.spi.ListenerHandler;

/**
 * The default configuration that only uses the {@link com.atlassian.event.internal.AnnotatedMethodsListenerHandler}.
 * <p>
 * Products that need to remain backward compatible will have to override this configuration
 */
public class ListenerHandlerConfigurationImpl implements ListenerHandlersConfiguration {
    @Override
    @Nonnull
    public List<ListenerHandler> getListenerHandlers() {
        return Lists.newArrayList(new AnnotatedMethodsListenerHandler());
    }
}
