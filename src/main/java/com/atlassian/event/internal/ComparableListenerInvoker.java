package com.atlassian.event.internal;

import java.util.Objects;
import java.util.Set;

import com.atlassian.event.spi.ListenerInvoker;

class ComparableListenerInvoker implements Comparable<ComparableListenerInvoker>, ListenerInvoker {
    private final ListenerInvoker delegate;
    private final int classHierarchyOrder;
    private final int registerOrder;

    ComparableListenerInvoker(ListenerInvoker listenerInvoker, int classHierarchyOrder, int registerOrder) {
        this.delegate = listenerInvoker;
        this.registerOrder = registerOrder;
        this.classHierarchyOrder = classHierarchyOrder;
    }

    @Override
    public int compareTo(ComparableListenerInvoker other) {
        int res = Integer.compare(getListenerOrder(), other.getListenerOrder());
        if (res != 0) {
            return res;
        }
        res = Integer.compare(getClassHierarchyOrder(), other.getClassHierarchyOrder());
        return res != 0 ? res : Integer.compare(getRegisterOrder(), other.getRegisterOrder());
    }

    private int getClassHierarchyOrder() {
        return classHierarchyOrder;
    }

    private int getRegisterOrder() {
        return registerOrder;
    }

    private int getListenerOrder() {
        return delegate.getOrder();
    }

    @Override
    public Set<Class<?>> getSupportedEventTypes() {
        return delegate.getSupportedEventTypes();
    }

    @Override
    public void invoke(Object event) {
        delegate.invoke(event);
    }

    @Override
    public boolean supportAsynchronousEvents() {
        return delegate.supportAsynchronousEvents();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ComparableListenerInvoker)) return false;
        ComparableListenerInvoker that = (ComparableListenerInvoker) o;
        return delegate.equals(that.delegate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(delegate);
    }
}
