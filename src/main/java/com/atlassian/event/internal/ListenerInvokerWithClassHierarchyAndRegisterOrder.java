package com.atlassian.event.internal;

final class ListenerInvokerWithClassHierarchyAndRegisterOrder {
    final ListenerInvokerWithRegisterOrder keyedListenerInvoker;
    final int classHierarchyOrder;

    ListenerInvokerWithClassHierarchyAndRegisterOrder(
            final ListenerInvokerWithRegisterOrder keyedListenerInvoker, final int classHierarchyOrder) {
        this.keyedListenerInvoker = keyedListenerInvoker;
        this.classHierarchyOrder = classHierarchyOrder;
    }

    ListenerInvokerWithRegisterOrder getListenerInvokerWithRegisterOrder() {
        return keyedListenerInvoker;
    }
}
