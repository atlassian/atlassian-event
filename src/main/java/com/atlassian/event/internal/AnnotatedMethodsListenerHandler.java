package com.atlassian.event.internal;

import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.spi.ListenerHandler;
import com.atlassian.event.spi.ListenerInvoker;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A listener handler that will check for single parameter methods annotated with the given annotation.
 * <p>
 * The default annotation for methods is {@link EventListener}.
 *
 * @see EventListener
 * @since 2.0
 */
public final class AnnotatedMethodsListenerHandler implements ListenerHandler {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private final Class annotationClass;

    public AnnotatedMethodsListenerHandler() {
        this(EventListener.class);
    }

    public AnnotatedMethodsListenerHandler(Class annotationClass) {
        this.annotationClass = checkNotNull(annotationClass);
    }

    public List<ListenerInvoker> getInvokers(final Object listener) {
        final Map<Method, EventListenerAnnotationParams> validMethods = getValidMethods(checkNotNull(listener));

        if (validMethods.isEmpty()) {
            log.debug(
                    "Couldn't find any valid listener methods on class <{}>",
                    listener.getClass().getName());
        }

        return validMethods.entrySet().stream()
                .map(entry -> new SingleParameterMethodListenerInvoker(
                        listener, entry.getKey(), entry.getValue().scope, entry.getValue().order))
                .collect(Collectors.toCollection(LinkedList::new));
    }

    private Map<Method, EventListenerAnnotationParams> getValidMethods(Object listener) {
        final Map<Method, EventListenerAnnotationParams> annotatedMethods = new LinkedHashMap<>();
        for (Method method : listener.getClass().getMethods()) {
            if (isValidMethod(method)) {
                annotatedMethods.put(method, getEventListenerAnnotationParams(method));
            }
        }
        return annotatedMethods;
    }

    private static class EventListenerAnnotationParams {
        final Optional<String> scope;
        final int order;

        private EventListenerAnnotationParams(final Optional<String> scope, final int order) {
            this.scope = scope;
            this.order = order;
        }
    }

    private EventListenerAnnotationParams getEventListenerAnnotationParams(Method method) {
        final EventListener annotation = method.getAnnotation(EventListener.class);
        final Optional<String> scope =
                Optional.ofNullable(annotation).map(EventListener::scope).filter(scopeName -> !"".equals(scopeName));
        final int order = annotation != null ? annotation.order() : 0;
        return new EventListenerAnnotationParams(scope, order);
    }

    private boolean isValidMethod(Method method) {
        if (isAnnotated(method)) {
            if (hasOneAndOnlyOneParameter(method)) {
                return true;
            } else {
                throw new RuntimeException(
                        "Method <" + method + "> of class <" + method.getDeclaringClass() + "> " + "is annotated with <"
                                + annotationClass.getName() + "> but has 0 or more than 1 parameters! "
                                + "Listener methods MUST have 1 and only 1 parameter.");
            }
        }
        return false;
    }

    @SuppressWarnings("unchecked")
    private boolean isAnnotated(Method method) {
        return method.getAnnotation(annotationClass) != null;
    }

    private boolean hasOneAndOnlyOneParameter(Method method) {
        return method.getParameterTypes().length == 1;
    }
}
