package com.atlassian.event.internal;

import javax.annotation.Nonnull;

import com.atlassian.event.api.AsynchronousPreferred;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Annotation based {@link AsynchronousEventResolver}. This will check whether the event is annotated with the given
 * annotation.
 * <p>
 * The default annotation used is {@link AsynchronousPreferred}
 *
 * @see AsynchronousPreferred
 * @since 2.0
 */
public final class AnnotationAsynchronousEventResolver implements AsynchronousEventResolver {
    private final Class annotationClass;

    AnnotationAsynchronousEventResolver() {
        this(AsynchronousPreferred.class);
    }

    AnnotationAsynchronousEventResolver(Class annotationClass) {
        this.annotationClass = checkNotNull(annotationClass);
    }

    @SuppressWarnings("unchecked")
    public boolean isAsynchronousEvent(@Nonnull Object event) {
        return checkNotNull(event).getClass().getAnnotation(annotationClass) != null;
    }
}
