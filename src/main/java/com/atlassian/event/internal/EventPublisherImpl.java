package com.atlassian.event.internal;

import java.util.Optional;
import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.event.config.ListenerHandlersConfiguration;
import com.atlassian.event.spi.EventDispatcher;
import com.atlassian.event.spi.ListenerHandler;
import com.atlassian.event.spi.ListenerInvoker;

import static java.util.Objects.requireNonNull;

/**
 * The default implementation of the {@link com.atlassian.event.api.EventPublisher} interface.
 * <p>
 * One can customise the event listening by instantiating with custom {@link ListenerHandler listener handlers} and
 * the event dispatching through {@link EventDispatcher}.
 * <p>
 * See the {@link com.atlassian.event.spi} package for more information.
 *
 * @see ListenerHandler
 * @see EventDispatcher
 * @since 2.0
 */
@ThreadSafe
public final class EventPublisherImpl implements EventPublisher {
    private static final Logger log = LoggerFactory.getLogger(EventPublisherImpl.class);

    private static final String PROPERTY_PREFIX = EventPublisherImpl.class.getName();

    static final Optional<String> debugRegistration =
            Optional.ofNullable(System.getProperty(PROPERTY_PREFIX + ".debugRegistration"));
    static final boolean debugRegistrationLocation = Boolean.getBoolean(PROPERTY_PREFIX + ".debugRegistrationLocation");
    static final Optional<String> debugInvocation =
            Optional.ofNullable(System.getProperty(PROPERTY_PREFIX + ".debugInvocation"));
    static final boolean debugInvocationLocation = Boolean.getBoolean(PROPERTY_PREFIX + ".debugInvocationLocation");

    private final ListenerRegistry listenerRegistry;
    private final EventDispatcher dispatcher;
    private final InvokerTransformer transformer;

    public EventPublisherImpl(
            final EventDispatcher eventDispatcher, final ListenerHandlersConfiguration configuration) {
        this(eventDispatcher, configuration, (invokers, event) -> invokers);
    }

    /**
     * If you need to customise the asynchronous handling, you should use the
     * {@link com.atlassian.event.internal.AsynchronousAbleEventDispatcher}
     * together with a custom executor.
     * <p>
     * You might also want to have a look at using the
     * {@link com.atlassian.event.internal.EventThreadFactory} to keep the naming
     * of event threads consistent with the default naming of the Atlassian Event
     * library.
     *
     * @param eventDispatcher               the event dispatcher to be used with the publisher
     * @param listenerHandlersConfiguration the list of listener handlers to be used with this publisher
     * @param transformer                   the batcher for batching up listener invocations
     * @see com.atlassian.event.internal.AsynchronousAbleEventDispatcher
     * @see com.atlassian.event.internal.EventThreadFactory
     */
    public EventPublisherImpl(
            final EventDispatcher eventDispatcher,
            final ListenerHandlersConfiguration listenerHandlersConfiguration,
            final InvokerTransformer transformer) {
        this.dispatcher = requireNonNull(eventDispatcher);
        this.transformer = requireNonNull(transformer);

        InvokerBuilder invokerBuilder = new InvokerBuilder(requireNonNull(listenerHandlersConfiguration));
        ListenerRegistry listenerRegistry = new ListenerRegistry(invokerBuilder);

        this.listenerRegistry = requireNonNull(listenerRegistry);
    }

    public void publish(final @Nonnull Object event) {
        requireNonNull(event);

        Iterable<ListenerInvoker> invokers = listenerRegistry.findListenerInvokers(event);
        try {
            invokers = transformer.transformAll(invokers, event);
        } catch (Exception e) {
            log.error("Exception while transforming invokers. Dispatching original invokers instead.", e);
        }
        final String eventClass = event.getClass().getName();
        final boolean debugThisInvocation =
                debugInvocation.map(eventClass::startsWith).orElse(false);

        for (final ListenerInvoker invoker : invokers) {
            // EVENT-14 -  we should continue to process all listeners even if one throws some horrible exception
            try {
                if (debugThisInvocation) {
                    log.warn("Listener invoked event with class '{}' -> invoker {}", eventClass, invoker);
                    if (debugInvocationLocation) {
                        log.warn("Invoked from", new Exception());
                    }
                }
                dispatcher.dispatch(invoker, event);
            } catch (Exception e) {
                log.error(
                        "There was an exception thrown trying to dispatch event '{}' from the invoker '{}'.",
                        event,
                        invoker,
                        e);
            }
        }
    }

    public void register(final @Nonnull Object listener) {
        requireNonNull(listener);
        unregister(listener);
        listenerRegistry.register(listener);
    }

    public void unregister(final @Nonnull Object listener) {
        listenerRegistry.remove(requireNonNull(listener));
    }

    public void unregisterAll() {
        listenerRegistry.clear();
    }
}
