package com.atlassian.event.internal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.ConcurrentMap;

import com.google.common.collect.MapMaker;

import com.atlassian.event.spi.ListenerInvoker;

/**
 * A collection of {@code ListenerInvoker} with the order of registration.
 */
class InvokerRegistry implements Iterable<InvokerRegistry.InvokerRegistration> {
    static final InvokerRegistry EMPTY = new InvokerRegistry();

    /**
     * <b>Warning:</b> We need to use weakKeys here to ensure plugin event classes are GC'd, otherwise we leak...
     */
    private final ConcurrentMap<Object, Collection<InvokerRegistration>> invokers =
            new MapMaker().weakKeys().makeMap();

    void remove(final Object listener) {
        invokers.remove(listener);
    }

    void add(final Object listener, final ListenerInvoker invoker, final int order) {
        invokers.computeIfAbsent(listener, k -> new ArrayList<>()).add(new InvokerRegistration(invoker, order));
    }

    @Override
    public Iterator<InvokerRegistration> iterator() {
        return invokers.values().stream().flatMap(Collection::stream).iterator();
    }

    static class InvokerRegistration {
        private final ListenerInvoker listenerInvoker;
        private final int order;

        InvokerRegistration(ListenerInvoker listenerInvoker, int order) {
            this.listenerInvoker = listenerInvoker;
            this.order = order;
        }

        ListenerInvoker getListenerInvoker() {
            return listenerInvoker;
        }

        int getOrder() {
            return order;
        }
    }
}
