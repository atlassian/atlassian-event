package com.atlassian.event.internal;

import java.util.List;

import com.google.common.collect.ImmutableList;

import com.atlassian.event.config.ListenerHandlersConfiguration;
import com.atlassian.event.spi.ListenerHandler;
import com.atlassian.event.spi.ListenerInvoker;
import com.atlassian.plugin.eventlistener.descriptors.EventListenerModuleDescriptor;

import static java.util.Objects.requireNonNull;

/**
 * Factory for  {@code ListenerInvoker}.
 */
class InvokerBuilder {
    private final Iterable<ListenerHandler> listenerHandlers;

    InvokerBuilder(ListenerHandlersConfiguration listenerHandlersConfiguration) {
        this.listenerHandlers = requireNonNull(listenerHandlersConfiguration.getListenerHandlers());
    }

    Iterable<ListenerInvoker> build(final Object listenerOrMd) throws IllegalArgumentException {
        final Object listener = getListener(listenerOrMd);
        final ImmutableList.Builder<ListenerInvoker> builder = ImmutableList.builder();
        for (final ListenerHandler listenerHandler : listenerHandlers) {
            builder.addAll(listenerHandler.getInvokers(listener));
        }
        final List<ListenerInvoker> invokers = builder.build();
        if (invokers.isEmpty()) {
            throw new IllegalArgumentException("No listener invokers were found for listener <" + listener + ">");
        }
        return invokers;
    }

    private Object getListener(final Object listenerOrMd) {
        if (listenerOrMd instanceof EventListenerModuleDescriptor) {
            final EventListenerModuleDescriptor descriptor = (EventListenerModuleDescriptor) listenerOrMd;
            return descriptor.getModule();
        } else {
            return listenerOrMd;
        }
    }
}
