package com.atlassian.event.internal;

import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

import com.atlassian.event.spi.ListenerInvoker;

final class ListenerInvokerWithRegisterOrder {
    private static final AtomicInteger registerOrderSequence = new AtomicInteger();

    private final Object listener;
    private final ListenerInvoker invoker;
    private final Optional<String> scope;
    private final int registerOrder;

    ListenerInvokerWithRegisterOrder(
            final Object listener, final ListenerInvoker invoker, final Optional<String> scope) {
        this.invoker = invoker;
        this.listener = listener;
        this.scope = scope;
        this.registerOrder = registerOrderSequence.incrementAndGet();
    }

    ListenerInvoker getInvoker() {
        return invoker;
    }

    Optional<String> getScope() {
        return scope;
    }

    int getOrder() {
        return invoker.getOrder();
    }

    public int getRegisterOrder() {
        return registerOrder;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof ListenerInvokerWithRegisterOrder)) return false;
        final ListenerInvokerWithRegisterOrder that = (ListenerInvokerWithRegisterOrder) o;
        return listener == that.listener && invoker.equals(that.invoker) && scope.equals(that.scope);
    }

    @Override
    public int hashCode() {
        return Objects.hash(System.identityHashCode(listener), invoker);
    }

    boolean isFor(final Object someListener) {
        return someListener == listener;
    }
}
