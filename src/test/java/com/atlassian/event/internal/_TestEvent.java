package com.atlassian.event.internal;

@SuppressWarnings("deprecation")
class _TestEvent {
    public _TestEvent(Object src) {}

    public static class TestFooSubEvent extends _TestEvent {
        public TestFooSubEvent(final Object src) {
            super(src);
        }
    }

    public static class TestBarSubEvent extends TestFooSubEvent {
        public TestBarSubEvent(final Object src) {
            super(src);
        }
    }

    public static class TestSubEvent extends _TestEvent {
        public TestSubEvent(final Object src) {
            super(src);
        }
    }

    public static class TestSubSubEvent extends TestSubEvent {
        public TestSubSubEvent(final Object src) {
            super(src);
        }
    }

    public static class TestInterfacedEvent implements TestSubInterface {
        public TestInterfacedEvent(final Object src) {}
    }

    public interface TestInterface {}

    public interface TestSubInterface extends TestInterface {}
}
