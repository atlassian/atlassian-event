package com.atlassian.event.internal;

import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import com.google.common.collect.Lists;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;

import static org.junit.Assert.assertEquals;

import static com.atlassian.event.internal._TestEvent.TestInterface;
import static com.atlassian.event.internal._TestEvent.TestInterfacedEvent;
import static com.atlassian.event.internal._TestEvent.TestSubEvent;
import static com.atlassian.event.internal._TestEvent.TestSubInterface;
import static com.atlassian.event.internal._TestEvent.TestSubSubEvent;

@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings("deprecation")
public class EventPublisherImplOrderingTest {

    private static final String OBJECT_LISTENER = "Object listener";
    private static final String SUBSUBEVENT_LISTENER = "TestSubSubEvent listener";
    private static final String SUBEVENT_LISTENER = "TestSubEvent listener";
    private static final String TESTEVENT_LISTENER = "TestEvent listener";
    private static final String INTERFACEIMPL_LISTENER = "TestInterfacedEvent listener";
    private static final String INTERFACE_LISTENER = "TestInterface listener";
    private static final String SUBINTERFACE_LISTENER = "TestSubInterface listener";

    private static class TestListener {
        final List<String> testEvents = Lists.newArrayList();

        @SuppressWarnings("unused")
        @EventListener
        public void onObjectEvent(final Object event) {
            testEvents.add(OBJECT_LISTENER);
        }

        @SuppressWarnings("unused")
        @EventListener
        public void onSubSubEvent(final TestSubSubEvent event) {
            testEvents.add(SUBSUBEVENT_LISTENER);
        }

        @SuppressWarnings("unused")
        @EventListener
        public void onSubEvent(final TestSubEvent event) {
            testEvents.add(SUBEVENT_LISTENER);
        }

        @SuppressWarnings("unused")
        @EventListener
        public void onEvent(final _TestEvent event) {
            testEvents.add(TESTEVENT_LISTENER);
        }

        @SuppressWarnings("unused")
        @EventListener
        public void onInterfacedEvent(final TestInterfacedEvent event) {
            testEvents.add(INTERFACEIMPL_LISTENER);
        }

        @SuppressWarnings("unused")
        @EventListener
        public void onInterface(final TestInterface event) {
            testEvents.add(INTERFACE_LISTENER);
        }

        @SuppressWarnings("unused")
        @EventListener
        public void onInterface(final TestSubInterface event) {
            testEvents.add(SUBINTERFACE_LISTENER);
        }
    }

    private final TestListener listener = new TestListener();

    private final EventPublisher eventPublisher = new EventPublisherImpl(
            new StubEventDispatcher(), () -> Collections.singletonList(new AnnotatedMethodsListenerHandler()));

    @Before
    public final void setUp() {
        eventPublisher.register(listener);
    }

    @Test
    public final void testInterfaceOrder() {
        List<String> expectedEvents =
                Lists.newArrayList(INTERFACEIMPL_LISTENER, OBJECT_LISTENER, SUBINTERFACE_LISTENER, INTERFACE_LISTENER);
        testOrder(new TestInterfacedEvent(this), expectedEvents);
    }

    @Test
    public final void testEventOrder() {
        List<String> expectedEvents =
                Lists.newArrayList(SUBSUBEVENT_LISTENER, SUBEVENT_LISTENER, TESTEVENT_LISTENER, OBJECT_LISTENER);
        testOrder(new TestSubSubEvent(this), expectedEvents);
    }

    private void testOrder(Object testEvent, List<String> orderedListeners) {
        eventPublisher.publish(testEvent);

        assertEquals(orderedListeners.size(), listener.testEvents.size());
        assertEquals(orderedListeners, listener.testEvents);
    }
}
