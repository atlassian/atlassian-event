package com.atlassian.event.internal;

import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import com.google.common.collect.Lists;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;

import static org.junit.Assert.assertEquals;

public class EventPublisherImplOrderingWithAnnotationTest {

    private static final String OBJECT_LISTENER_ORDER_MIN_INFINITY = "Object listener - min infinity";
    private static final String SUBSUBEVENT_LISTENER_ORDER_PLUS_100 = "TestSubSubEvent listener - plus 100";
    private static final String SUBEVENT_LISTENER_ORDER_MIN_100 = "TestSubEvent listener - min 100";
    private static final String FOO_SUBEVENT_LISTENER_BEFORE = "TestFooSubEvent listener - before";
    private static final String FOO_SUBEVENT_LISTENER_DEFAULT = "TestFooSubEvent listener - default";
    private static final String FOO_SUBEVENT_LISTENER_AFTER = "TestFooSubEvent listener - after";
    private static final String BAR_SUBEVENT_LISTENER_BEFORE = "TestBarSubEvent listener - before";
    private static final String BAR_SUBEVENT_LISTENER_DEFAULT = "TestBarSubEvent listener - default";
    private static final String BAR_SUBEVENT_LISTENER_AFTER = "TestBarSubEvent listener - after";
    private static final String TESTEVENT_LISTENER_ORDER_DEFAULT = "TestEvent listener - default";
    private static final String TESTEVENT_LISTENER_2_ORDER_DEFAULT = "TestEvent listener2 - default";
    private static final String INTERFACEIMPL_LISTENER_ORDER_DEFAULT = "TestInterfacedEvent listener - default";
    private static final String INTERFACE_LISTENER_ORDER_DEFAULT = "TestInterface listener - default";
    private static final String SUBINTERFACE_LISTENER_ORDER_DEFAULT = "TestSubInterface listener - default";
    private static final String FOO_SUBEVENT_LISTENER_2_BEFORE = "TestFooSubEvent listener2 - before";
    private static final String FOO_SUBEVENT_LISTENER_2_DEFAULT = "TestFooSubEvent listener2 - default";
    private static final String FOO_SUBEVENT_LISTENER_2_AFTER = "TestFooSubEvent listener2 - after";

    private class TestListener {

        @SuppressWarnings("unused")
        @EventListener(order = Integer.MIN_VALUE)
        public void onObjectEvent(final Object event) {
            testEvents.add(OBJECT_LISTENER_ORDER_MIN_INFINITY);
        }

        @SuppressWarnings("unused")
        @EventListener(order = 100)
        public void onSubSubEvent(final _TestEvent.TestSubSubEvent event) {
            testEvents.add(SUBSUBEVENT_LISTENER_ORDER_PLUS_100);
        }

        @SuppressWarnings("unused")
        @EventListener(order = -100)
        public void onSubEvent(final _TestEvent.TestSubEvent event) {
            testEvents.add(SUBEVENT_LISTENER_ORDER_MIN_100);
        }

        @SuppressWarnings("unused")
        @EventListener(order = -10)
        public void onFooSubEventBefore(final _TestEvent.TestFooSubEvent event) {
            testEvents.add(FOO_SUBEVENT_LISTENER_BEFORE);
        }

        @SuppressWarnings("unused")
        @EventListener
        public void onFooSubEventDefault(final _TestEvent.TestFooSubEvent event) {
            testEvents.add(FOO_SUBEVENT_LISTENER_DEFAULT);
        }

        @SuppressWarnings("unused")
        @EventListener(order = 10)
        public void onFooSubEventAfter(final _TestEvent.TestFooSubEvent event) {
            testEvents.add(FOO_SUBEVENT_LISTENER_AFTER);
        }

        @SuppressWarnings("unused")
        @EventListener(order = -10)
        public void onBarSubEventBefore(final _TestEvent.TestBarSubEvent event) {
            testEvents.add(BAR_SUBEVENT_LISTENER_BEFORE);
        }

        @SuppressWarnings("unused")
        @EventListener
        public void onBarSubEventDefault(final _TestEvent.TestBarSubEvent event) {
            testEvents.add(BAR_SUBEVENT_LISTENER_DEFAULT);
        }

        @SuppressWarnings("unused")
        @EventListener(order = 10)
        public void onBarSubEventAfter(final _TestEvent.TestBarSubEvent event) {
            testEvents.add(BAR_SUBEVENT_LISTENER_AFTER);
        }

        @SuppressWarnings("unused")
        @EventListener
        public void onEvent(final _TestEvent event) {
            testEvents.add(TESTEVENT_LISTENER_ORDER_DEFAULT);
        }

        @SuppressWarnings("unused")
        @EventListener
        public void onInterfacedEvent(final _TestEvent.TestInterfacedEvent event) {
            testEvents.add(INTERFACEIMPL_LISTENER_ORDER_DEFAULT);
        }

        @SuppressWarnings("unused")
        @EventListener
        public void onInterface(final _TestEvent.TestInterface event) {
            testEvents.add(INTERFACE_LISTENER_ORDER_DEFAULT);
        }

        @SuppressWarnings("unused")
        @EventListener
        public void onInterface(final _TestEvent.TestSubInterface event) {
            testEvents.add(SUBINTERFACE_LISTENER_ORDER_DEFAULT);
        }
    }

    private class TestListener2 {
        @SuppressWarnings("unused")
        @EventListener(order = -10)
        public void onFooSubEventBefore(final _TestEvent.TestFooSubEvent event) {
            testEvents.add(FOO_SUBEVENT_LISTENER_2_BEFORE);
        }

        @SuppressWarnings("unused")
        @EventListener
        public void onFooSubEventDefault(final _TestEvent.TestFooSubEvent event) {
            testEvents.add(FOO_SUBEVENT_LISTENER_2_DEFAULT);
        }

        @SuppressWarnings("unused")
        @EventListener(order = 10)
        public void onFooSubEventAfter(final _TestEvent.TestFooSubEvent event) {
            testEvents.add(FOO_SUBEVENT_LISTENER_2_AFTER);
        }

        @SuppressWarnings("unused")
        @EventListener
        public void onEvent(final _TestEvent event) {
            testEvents.add(TESTEVENT_LISTENER_2_ORDER_DEFAULT);
        }
    }

    private final TestListener listener = new TestListener();
    private List<String> testEvents;

    private final EventPublisher eventPublisher = new EventPublisherImpl(
            new StubEventDispatcher(), () -> Collections.singletonList(new AnnotatedMethodsListenerHandler()));

    @Before
    public final void setUp() {
        testEvents = Lists.newArrayList();
        eventPublisher.register(listener);
    }

    @Test
    public final void testInterfaceOrder() {
        List<String> expectedEvents = Lists.newArrayList(
                OBJECT_LISTENER_ORDER_MIN_INFINITY,
                INTERFACEIMPL_LISTENER_ORDER_DEFAULT,
                SUBINTERFACE_LISTENER_ORDER_DEFAULT,
                INTERFACE_LISTENER_ORDER_DEFAULT);
        testOrder(new _TestEvent.TestInterfacedEvent(this), expectedEvents);
    }

    @Test
    public final void testEventOrder() {
        List<String> expectedEvents = Lists.newArrayList(
                OBJECT_LISTENER_ORDER_MIN_INFINITY,
                SUBEVENT_LISTENER_ORDER_MIN_100,
                TESTEVENT_LISTENER_ORDER_DEFAULT,
                SUBSUBEVENT_LISTENER_ORDER_PLUS_100);
        testOrder(new _TestEvent.TestSubSubEvent(this), expectedEvents);
    }

    @Test
    public final void testEventFoo() {
        List<String> expectedEvents = Lists.newArrayList(
                OBJECT_LISTENER_ORDER_MIN_INFINITY,
                FOO_SUBEVENT_LISTENER_BEFORE,
                FOO_SUBEVENT_LISTENER_DEFAULT,
                TESTEVENT_LISTENER_ORDER_DEFAULT,
                FOO_SUBEVENT_LISTENER_AFTER);
        testOrder(new _TestEvent.TestFooSubEvent(this), expectedEvents);
    }

    @Test
    public final void testEventBar() {
        List<String> expectedEvents = Lists.newArrayList(
                OBJECT_LISTENER_ORDER_MIN_INFINITY,
                BAR_SUBEVENT_LISTENER_BEFORE,
                FOO_SUBEVENT_LISTENER_BEFORE,
                BAR_SUBEVENT_LISTENER_DEFAULT,
                FOO_SUBEVENT_LISTENER_DEFAULT,
                TESTEVENT_LISTENER_ORDER_DEFAULT,
                BAR_SUBEVENT_LISTENER_AFTER,
                FOO_SUBEVENT_LISTENER_AFTER);
        testOrder(new _TestEvent.TestBarSubEvent(this), expectedEvents);
    }

    @Test
    public void testTwoListenersSameClassFoo() {
        eventPublisher.register(new TestListener());
        List<String> expectedEvents = Lists.newArrayList(
                OBJECT_LISTENER_ORDER_MIN_INFINITY,
                OBJECT_LISTENER_ORDER_MIN_INFINITY,
                FOO_SUBEVENT_LISTENER_BEFORE,
                FOO_SUBEVENT_LISTENER_BEFORE,
                FOO_SUBEVENT_LISTENER_DEFAULT,
                FOO_SUBEVENT_LISTENER_DEFAULT,
                TESTEVENT_LISTENER_ORDER_DEFAULT,
                TESTEVENT_LISTENER_ORDER_DEFAULT,
                FOO_SUBEVENT_LISTENER_AFTER,
                FOO_SUBEVENT_LISTENER_AFTER);
        testOrder(new _TestEvent.TestFooSubEvent(this), expectedEvents);
    }

    @Test
    public void testTwoListenersDifferentClassFooRespectClassHierarchyOrderAndAddOrder() {
        eventPublisher.register(new TestListener2());
        List<String> expectedEvents = Lists.newArrayList(
                OBJECT_LISTENER_ORDER_MIN_INFINITY,
                FOO_SUBEVENT_LISTENER_BEFORE,
                FOO_SUBEVENT_LISTENER_2_BEFORE,
                FOO_SUBEVENT_LISTENER_DEFAULT,
                FOO_SUBEVENT_LISTENER_2_DEFAULT,
                TESTEVENT_LISTENER_ORDER_DEFAULT,
                TESTEVENT_LISTENER_2_ORDER_DEFAULT,
                FOO_SUBEVENT_LISTENER_AFTER,
                FOO_SUBEVENT_LISTENER_2_AFTER);
        testOrder(new _TestEvent.TestFooSubEvent(this), expectedEvents);
    }

    @Test
    public void testTwoListenersDifferentClassFooRespectClassHierarchyOrderAndAddOrderReversed() {
        eventPublisher.unregister(listener);
        eventPublisher.register(new TestListener2());
        eventPublisher.register(listener);
        List<String> expectedEvents = Lists.newArrayList(
                OBJECT_LISTENER_ORDER_MIN_INFINITY,
                FOO_SUBEVENT_LISTENER_2_BEFORE,
                FOO_SUBEVENT_LISTENER_BEFORE,
                FOO_SUBEVENT_LISTENER_2_DEFAULT,
                FOO_SUBEVENT_LISTENER_DEFAULT,
                TESTEVENT_LISTENER_2_ORDER_DEFAULT,
                TESTEVENT_LISTENER_ORDER_DEFAULT,
                FOO_SUBEVENT_LISTENER_2_AFTER,
                FOO_SUBEVENT_LISTENER_AFTER);
        testOrder(new _TestEvent.TestFooSubEvent(this), expectedEvents);
    }

    private void testOrder(Object testEvent, List<String> orderedListeners) {
        eventPublisher.publish(testEvent);

        assertEquals(orderedListeners.size(), testEvents.size());
        assertEquals(orderedListeners, testEvents);
    }
}
