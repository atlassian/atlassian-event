package com.atlassian.event.internal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.event.spi.ListenerInvoker;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ComparableListenerInvokerTest {
    @Mock
    private ListenerInvoker listenerInvokerA;

    @Mock
    private ListenerInvoker listenerInvokerB;

    @Test
    public void testListenerOrder() {
        when(listenerInvokerA.getOrder()).thenReturn(0);
        when(listenerInvokerB.getOrder()).thenReturn(1);

        ComparableListenerInvoker a = new ComparableListenerInvoker(listenerInvokerA, 2, 1);
        ComparableListenerInvoker b = new ComparableListenerInvoker(listenerInvokerB, 0, 0);
        assertThat("listener order", a.compareTo(b) < 0);
    }

    @Test
    public void testClassHierarchyOrder() {
        when(listenerInvokerA.getOrder()).thenReturn(1);
        when(listenerInvokerB.getOrder()).thenReturn(1);

        ComparableListenerInvoker a = new ComparableListenerInvoker(listenerInvokerA, 2, 1);
        ComparableListenerInvoker b = new ComparableListenerInvoker(listenerInvokerB, 3, 0);
        assertThat("class hierarchy order", a.compareTo(b) < 0);
    }

    @Test
    public void testRegistrationOrder() {
        when(listenerInvokerA.getOrder()).thenReturn(1);
        when(listenerInvokerB.getOrder()).thenReturn(1);

        ComparableListenerInvoker a = new ComparableListenerInvoker(listenerInvokerA, 2, 1);
        ComparableListenerInvoker b = new ComparableListenerInvoker(listenerInvokerB, 2, 0);
        assertThat("registration order", a.compareTo(b) > 0);
    }
}
