package com.atlassian.event.internal;

import javax.annotation.Nonnull;

import com.atlassian.event.spi.EventDispatcher;
import com.atlassian.event.spi.ListenerInvoker;

/**
 * A stub event dispatcher that simply calls the invoker.
 */
final class StubEventDispatcher implements EventDispatcher {
    public void dispatch(@Nonnull ListenerInvoker invoker, @Nonnull Object event) {
        invoker.invoke(event);
    }
}
