# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased][unreleased]

## [4.1.0][4_1_0] - 2019-12-13
### Changed
- Promote the lock free event publisher to be the default implementation  ([DCNG-301](http://go/j/DCNG-301))
- Cache listener invokers for an event class ([DCNG-301](http://go/j/DCNG-301))

## [4.0.3][4_0_3] - 2020-11-24
### Changed
- updated platform POM dependency from 5.0.17 to 5.0.27 ([BSP-1958](http://go/j/BSP-1958))

## [4.0.2][4_0_2] - 2019-11-23
### Added
- SourceClear integration ([BSP-559](http://go/j/BSP-559))
### Changed
- updated platform POM dependency to a final 5.0.0 ([EVENT-45](http://go/j/EVENT-45))
- updated platform POM dependency from 5.0.0 to 5.0.17 ([EVENT-47](http://go/j/EVENT-47))
### Fixed
- fixed high contention in EventPublisher during stress test ([DCNG-301](http://go/j/DCNG-301))

## [4.0.1][4_0_1] - 2019-07-01
### Added
- added information about project ownership ([BSP-271](http://go/j/BSP-271))
- synchronous event listeners can suggest order in which they are called ([DELTA-859](http://go/j/DELTA-859))
### Changed
- POM cleanup ([BSP-283](http://go/j/BSP-283))

## [4.0.0][4_0_0] - 2018-11-26
### Added
- Java 10 compatibility ([EVENT-44](http://go/j/EVENT-44))
- SourceClear integration ([BSP-559](http://go/j/BSP-559))
### Changed
- Platform 5 ([EVENT-45](http://go/j/EVENT-45))
- remove annotation-api dependency, use spring `@Autowired` instead ([HL-694](http://go/j/HL-694))

## [3.1.5][3_1_5] - 2017-01-30
### Fixed
- 3.1.4 release was borked 

## [3.1.4][3_1_4] - 2017-01-30
### Removed
- removed usage of deprecated Guava API `MoreExecutors.sameThreadExecutor()` ([EVENT-43](http://go/j/EVENT-43))

## [3.1.3][3_1_3] - 2016-05-16
### Added
- add ability to specify scope of event listener ([EVENT-38](http://go/j/EVENT-38))

## [3.1.2][3_1_2] - 2016-05-04
### Changed
- updated platform POM dependency from 3.2.17 to 3.2.20 ([VPP-243](http://go/j/VPP-243))
- more debug information in exceptions ([JDEV-36890](http://go/j/JDEV-36890))
### Removed
- removed dependency on `com.atlassian.plugins:atlassian-plugins-eventlistener` ([VPP-243](http://go/j/VPP-243))

## [3.1.1][3_1_1] - 2016-05-03
### Added
- Project Vertigo: make event listener module 0-tenant safe ([VPP-243](http://go/j/VPP-243))
### Changed
- updated platform POM dependency from 3.0.13 to 3.2.17 ([EVENT-37](http://go/j/EVENT-37))

## [3.1.0][3_1_0] - 2016-04-20
### Added
- additional logging for registration and invocation ([EVENT-36](http://go/j/PLATFORM-159))
### Changed
- updated code style ([PLATFORM-159](http://go/j/PLATFORM-159))
- added pom-cleaning wizard ([PLATDEV-20](http://go/j/PLATDEV-20))
### Removed
- removed compatibility with pre-8 JDKs ([EVENT-35](http://go/j/EVENT-35))

[unreleased]: https://bitbucket.org/atlassian/atlassian-event/branches/compare/master%0Datlassian-event-4.0.2
[4_0_3]: https://bitbucket.org/atlassian/atlassian-event/branches/compare/atlassian-event-4.0.3%0Datlassian-event-4.0.2
[4_0_2]: https://bitbucket.org/atlassian/atlassian-event/branches/compare/atlassian-event-4.0.2%0Datlassian-event-4.0.1
[4_0_1]: https://bitbucket.org/atlassian/atlassian-event/branches/compare/atlassian-event-4.0.1%0Datlassian-event-4.0.0
[4_0_0]: https://bitbucket.org/atlassian/atlassian-event/branches/compare/atlassian-event-4.0.0%0Datlassian-event-3.1.x
[3_1_5]: https://bitbucket.org/atlassian/atlassian-event/branches/compare/atlassian-event-3.1.5%0Datlassian-event-3.1.4
[3_1_4]: https://bitbucket.org/atlassian/atlassian-event/branches/compare/atlassian-event-3.1.4%0Datlassian-event-3.1.3
[3_1_3]: https://bitbucket.org/atlassian/atlassian-event/branches/compare/atlassian-event-3.1.3%0Datlassian-event-3.1.2
[3_1_2]: https://bitbucket.org/atlassian/atlassian-event/branches/compare/atlassian-event-3.1.2%0Datlassian-event-3.1.1
[3_1_1]: https://bitbucket.org/atlassian/atlassian-event/branches/compare/atlassian-event-3.1.1%0Datlassian-event-3.1.0
[3_1_0]: https://bitbucket.org/atlassian/atlassian-event/branches/compare/atlassian-event-3.1.0%0Datlassian-event-3.0.x