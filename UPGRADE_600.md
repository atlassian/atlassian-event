# Upgrade guide for 6.0.0

## Code deprecations

| Class name                                                       | Code                                                                               | Alternative code                                                     | Migration notes                                                                    |
|------------------------------------------------------------------|------------------------------------------------------------------------------------|----------------------------------------------------------------------|------------------------------------------------------------------------------------|
| `com.atlassian.event.Event`                                      | Whole class                                                                        | -                                                                    | See below                                                                          |
| `com.atlassian.event.EventListener`                              | Whole class                                                                        | -                                                                    | See below                                                                          |
| `com.atlassian.event.EventManager`                               | Whole class                                                                        | -                                                                    | See below                                                                          |
| `com.atlassian.event.legacy.LegacyEventManager`                  | Whole class                                                                        | -                                                                    | See below                                                                          |
| `com.atlassian.event.legacy.LegacyListenerHandler`               | Whole class                                                                        | -                                                                    | See below                                                                          |
| `com.atlassian.event.legacy.SpringContextEventPublisher`         | Whole class                                                                        | -                                                                    | See below                                                                          |
| `com.atlassian.event.spring.EventListenerRegistrarBeanProcessor` | Whole class                                                                        | -                                                                    | See below                                                                          |
| `com.atlassian.event.internal.EventExecutorFactoryImpl`          | Whole class                                                                        | `DirectEventExecutorFactory`                                         | -                                                                                  |
| `com.atlassian.event.internal.EventPublisherImpl`                | `EventPublisherImpl(EventDispatcher, ListenerHandlersConfiguration, ScopeManager)` | `EventPublisherImpl(EventDispatcher, ListenerHandlersConfiguration)` | `ScopeManager` is already ignored. Remove it as parameter to the constructor call. |
| `com.atlassian.event.internal.LockFreeEventPublisher`            | Whole class                                                                        | `com.atlassian.event.internal.EventPublisherImpl`                    | -                                                                                  |                                                                                |

## Annotation based events
Ever since atlassian-event 2.0, we have switched to an annotation based event listening system. This means there is no longer a need to implement the interfaces that were part of the public API. Read the product-specific documentation for the best practices on implementing event listeners for that product. There is likely no change needed.

* **Jira:** [Writing Jira event listeners with the atlassian-event library](https://developer.atlassian.com/server/jira/platform/writing-jira-event-listeners-with-the-atlassian-event-library/)
* **Confluence:** [Event Listener module](https://developer.atlassian.com/server/confluence/event-listener-module/)
* **Bitbucket:** [Responding to application events](https://developer.atlassian.com/server/bitbucket/how-tos/responding-to-application-events/)
* **Bamboo:** [Bamboo Event Listeners](https://developer.atlassian.com/server/bamboo/bamboo-event-listeners/)
* **Crowd:** [Event listeners](https://developer.atlassian.com/server/crowd/event-listeners/) 
