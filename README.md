# Atlassian Event

## Description

Standard event listening, registration, and publication interface, along with basic implementations.

## Ownership

See [Atlassian Event](https://hello.atlassian.net/compass/component/de694413-e43a-4600-b528-a1f9868947de) on Compass.

## Atlassian Developer?

### Committing Guidelines

Please see [The Platform Rules of Engagement (go/proe)](http://go.atlassian.com/proe) for committing to this module.

### Builds

See [Atlassian Event](https://hello.atlassian.net/compass/component/de694413-e43a-4600-b528-a1f9868947de) on Compass.

## External User?

### Issues

Please raise any issues you find with this module in [JIRA](https://ecosystem.atlassian.net/browse/EVENT)

### Documentation

- [JIRA Event Listener Documentation](https://developer.atlassian.com/x/BgIr)
- [Confluence Event Listener Documentation](https://developer.atlassian.com/x/2gAf)
